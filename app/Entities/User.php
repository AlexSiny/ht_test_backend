<?php

namespace App\Entities;

use Core\Database\Entity;

class User extends Entity
{
    protected $location;

    public static function getPrimaryKey(): string
    {
        return 'uuid';
    }

    public function getAvailableAttributes(): array
    {
        return [
            'uuid',
            'first_name',
            'last_name',
            'email',
            'phone',
            'registered_at',
        ];
    }

    public function setLocation(Location $location)
    {
        $this->location = $location;
    }

    public function getLocation(): Location
    {
        return $this->location;
    }
}
