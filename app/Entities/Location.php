<?php

namespace App\Entities;

use Core\Database\Entity;

class Location extends Entity
{
    public static function getPrimaryKey(): string
    {
        return 'uuid';
    }

    public function getAvailableAttributes(): array
    {
        return [
            'uuid',
            'street',
            'city',
            'state',
            'postcode',
            'coordinates',
            'timezone',
        ];
    }

    public function getAddress(): string
    {
        $addressParts = [
            'postcode',
            'state',
            'city',
            'street',
        ];

        $address = [];

        foreach ($addressParts as $partKey) {
            if ($partVal = $this->getAttribute($partKey)) {
                $address[$partKey] = $partVal;
            }
        }

        return implode(', ', $address);
    }
}
