<?php

namespace App\Controllers;

use App\Entities\Location;
use App\Entities\User;
use App\Repositories\FileDbUserRepository;
use App\Repositories\SQLiteUserRepository;
use Core\Controller\Controller;
use Core\Database\Connectors\FileDbConnection;
use Core\Database\Connectors\SQLiteConnection;
use Core\Database\Repositories\RepositoryInterface;
use Core\Utility\Faker;
use Monolog\Handler\RotatingFileHandler;
use Monolog\Logger;
use Nette\Caching\Cache;
use Nette\Caching\Storages\FileStorage;
use Pecee\SimpleRouter\Exceptions\NotFoundHttpException;

class UsersController extends Controller
{
    private $cache;

    private $logger;

    private $usersCacheKey = 'db_users';

    public function __construct()
    {
        parent::__construct();

        $cacheStorage = new FileStorage(cache_path());
        $this->cache = new Cache($cacheStorage);

        $logger = new Logger('general');
        $loggerHandler = new RotatingFileHandler(logs_path('users.log'), 5);

        $this->logger = $logger->pushHandler($loggerHandler);
    }

    public function showFromFileDbStorage()
    {
        $connection = FileDbConnection::getInstance();
        $repository = new FileDbUserRepository($connection);

        $users = $repository->getAll();

        $this->logger->info('Список пользователей загружен из файловой базы данных');

        $this->view
            ->setTitle('Список пользователей хранящихся в файле')
            ->render('users.list_from_file', [
                'users' => $users,
            ]);
    }

    /**
     * @param string $userId
     * @throws NotFoundHttpException
     */
    public function removeFromFileDbStorage(string $userId)
    {
        $connection = FileDbConnection::getInstance();
        $repository = new FileDbUserRepository($connection);

        $user = $repository->getById($userId);

        if (empty($user)) {
            throw new NotFoundHttpException(sprintf('Пользователь с uuid "%s" отсутствует!', $userId));
        }

        $repository->delete($user);

        $this->logger->info("Пользователь с uuid '{$userId}' удален из файловой базы данных");

        response()->redirect(route('users.file.list'));
    }

    public function generateForFileDbStorage()
    {
        $connection = FileDbConnection::getInstance();
        $repository = new FileDbUserRepository($connection);

        $this->generateUsers($repository);

        $this->logger->info('Сгенерированы пользователи для файловой базы данных');

        response()->redirect(route('users.file.list'));
    }

    public function showFromSQLiteStorage()
    {
        if ($users = $this->cache->load($this->usersCacheKey)) {
            $this->logger->info('Список пользователей загружен из кэша вместо использования SQLite базы данных');
        }

        if (empty($users)) {
            $connection = SQLiteConnection::getInstance();
            $repository = new SQLiteUserRepository($connection);

            $users = $repository->getAll();

            $this->cache->save($this->usersCacheKey, $users);

            $this->logger->info('Список пользователей загружен из SQLite базы данных и помещен в кэш');
        }

        $this->view
            ->setTitle('Список пользователей хранящихся в базе данных')
            ->render('users.list_from_db', [
                'users' => $users,
            ]);
    }

    /**
     * @param string $userId
     * @throws NotFoundHttpException
     */
    public function removeFromSQLiteStorage(string $userId)
    {
        $connection = SQLiteConnection::getInstance();
        $repository = new SQLiteUserRepository($connection);

        $user = $repository->getById($userId);

        if (empty($user)) {
            throw new NotFoundHttpException(sprintf('Пользователь с uuid "%s" отсутствует!', $userId));
        }

        $repository->delete($user);

        $this->cache->remove($this->usersCacheKey);

        $this->logger->info("Пользователь с uuid '{$userId}' удален из SQLite базы данных");

        response()->redirect(route('users.db.list'));
    }

    public function generateForSQLiteStorage()
    {
        $connection = SQLiteConnection::getInstance();
        $repository = new SQLiteUserRepository($connection);

        $this->generateUsers($repository);

        $this->cache->remove($this->usersCacheKey);

        $this->logger->info('Сгенерированы пользователи для SQLite базы данных');

        response()->redirect(route('users.db.list'));
    }

    private function generateUsers(RepositoryInterface $repository)
    {
        $faker = new Faker();

        $usersWithLocation = $faker->generateUsersWithLocation();

        $repository->truncate();

        foreach ($usersWithLocation as $data) {
            $user = new User($data['user']);
            $location = new Location($data['location']);

            $user->setLocation($location);

            $repository->add($user);
        }
    }
}
