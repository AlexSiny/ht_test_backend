<?php

namespace App\Repositories;

use App\Entities\Location;
use App\Entities\User;
use Core\Database\Connectors\SQLiteConnection;
use Core\Database\EntityInterface;
use Core\Database\Repositories\SQLiteRepository;

class SQLiteUserRepository extends SQLiteRepository
{
    public function __construct(SQLiteConnection $connection)
    {
        parent::__construct($connection);

        $this->createTableIfNotExists();
    }

    public function getEntityClass(): string
    {
        return User::class;
    }

    public function getTableName(): string
    {
        return 'users';
    }

    public function createTableIfNotExists()
    {
        $this->getPdo()->exec("
            CREATE TABLE IF NOT EXISTS {$this->getTableName()} (
              uuid VARCHAR(255) NOT NULL,
              first_name VARCHAR(255) NOT NULL,
              last_name VARCHAR(255) NOT NULL,
              phone VARCHAR(255) DEFAULT NULL,
              email VARCHAR(255) DEFAULT NULL,
              location TEXT NOT NULL,
              registered_at DATETIME NOT NULL,
              PRIMARY KEY (uuid)
            )
        ");
    }

    protected function createEntity(array $attributes): EntityInterface
    {
        if (!empty($attributes['location'])) {
            $location = new Location(json_decode($attributes['location'], true));
            unset($attributes['location']);
        }

        if (!empty($attributes['registered_at'])) {
            $attributes['registered_at'] = date_create($attributes['registered_at']);
        }

        /** @var User $user */
        $user = parent::createEntity($attributes);

        if (isset($location)) {
            $user->setLocation($location);
        }

        return $user;
    }

    protected function processingEntityForSave(EntityInterface $entity): array
    {
        /** @var User $entity */
        $processed = parent::processingEntityForSave($entity);

        if (!empty($processed['registered_at'])) {
            $processed['registered_at'] = date('Y-m-d H:i:s', $processed['registered_at']->getTimestamp());
        }

        if ($entity->getLocation()) {
            $processed['location'] = json_encode($entity->getLocation()->toArray());
        }

        return $processed;
    }
}
