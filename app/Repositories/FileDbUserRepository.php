<?php

namespace App\Repositories;

use App\Entities\Location;
use App\Entities\User;
use Core\Database\EntityInterface;
use Core\Database\Repositories\FileDbRepository;

class FileDbUserRepository extends FileDbRepository
{
    public function getEntityClass(): string
    {
        return User::class;
    }

    public function getFileName(): string
    {
        return 'users';
    }

    protected function createEntity(array $attributes): EntityInterface
    {
        /** @var User $user */
        $user = parent::createEntity($attributes['user']);

        if (isset($attributes['location'])) {
            $location = new Location($attributes['location']);
            $user->setLocation($location);
        }

        return $user;
    }

    protected function processingEntityForSave(EntityInterface $entity): array
    {
        /** @var User $entity */
        $processed['user'] = parent::processingEntityForSave($entity);

        if ($entity->getLocation()) {
            $processed['location'] = $entity->getLocation()->toArray();
        }

        return $processed;
    }
}
