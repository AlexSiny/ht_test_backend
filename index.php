<?php

use Pecee\SimpleRouter\SimpleRouter as Router;

error_reporting(E_ERROR);

require_once 'vendor/autoload.php';
require_once 'core/helpers.php';
require_once 'configs/routes.php';

Router::start();
