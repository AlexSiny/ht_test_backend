<?php

namespace Core\Utility;

class Faker
{
    const API_URL = 'https://randomuser.me/api/';

    public function generateUsersWithLocation(int $limit = 5, string $nat = 'gb'): array
    {
        $apiUrl = self::API_URL . "?results={$limit}&nat={$nat}";
        $rawData = $this->callApi($apiUrl);

        $result = [];

        foreach ($rawData as $rawItem) {
            $result[] = [
                'user' => [
                    'uuid' => uniqid(),
                    'first_name' =>  $rawItem['name']['first'],
                    'last_name' =>  $rawItem['name']['last'],
                    'email' => $rawItem['email'],
                    'phone' => $rawItem['phone'],
                    'registered_at' => date_create($rawItem['registered']['date']),
                ],
                'location' => [
                    'uuid' => uniqid(),
                    'street' => $rawItem['location']['street'],
                    'city' => $rawItem['location']['city'],
                    'state' => $rawItem['location']['state'],
                    'postcode' => $rawItem['location']['postcode'],
                    'coordinates' => $rawItem['location']['coordinates'],
                    'timezone' => $rawItem['location']['timezone'],
                ],
            ];
        }

        return $result;
    }

    private function callApi(string $url)
    {
        $data = file_get_contents($url);

        return json_decode($data, true)['results'];
    }
}
