<?php

namespace Core\Utility;

class Config
{
    private $data = [];

    public function __construct(string $configFileName)
    {
        $configFilePath = configs_path("{$configFileName}.php");

        if (!file_exists($configFilePath)) {
            throw new \InvalidArgumentException(
                sprintf('Конфигурационный файл "%s" отсутствует!', $configFilePath)
            );
        }

        $this->data = include($configFilePath);
    }

    public function get(string $key, $default = null)
    {
        return $this->data[$key] ?? $default;
    }
}
