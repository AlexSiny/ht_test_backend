<?php

namespace Core\Controller;

use Core\View\View;

class Controller
{
    protected $view;

    public function __construct()
    {
        $this->view = new View(views_path(), 'default');
    }
}
