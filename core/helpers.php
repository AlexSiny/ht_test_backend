<?php

use Pecee\Http\Response;
use Pecee\Http\Request;
use Pecee\SimpleRouter\SimpleRouter as Router;

function dump($var)
{
    echo '<pre>' . print_r($var, true) . '</pre>';
}

function route(string $name = null, $parameters = null, array $getParams = null): string
{
    $url = Router::getUrl($name, $parameters, $getParams);
    return $url->getRelativeUrl();
}

function response(): Response
{
    return Router::response();
}

function request(): Request
{
    return Router::request();
}

function configs_path(string $path = '')
{
    return realpath(__DIR__ . '/../configs') . '/' . ltrim($path, '/');
}

function views_path(string $path = '')
{
    return realpath(__DIR__ . '/../resources/views') . '/' . ltrim($path, '/');
}

function cache_path(string $path = '')
{
    return realpath(__DIR__ . '/../storage/cache') . '/' . ltrim($path, '/');
}

function logs_path(string $path = '')
{
    return realpath(__DIR__ . '/../storage/logs') . '/' . ltrim($path, '/');
}

function databases_path(string $path = '')
{
    return realpath(__DIR__ . '/../storage/databases') . '/' . ltrim($path, '/');
}

function rus_date()
{
    $translate = [
        'am' => 'дп',
        'pm' => 'пп',
        'AM' => 'ДП',
        'PM' => 'ПП',
        'Monday' => 'Понедельник',
        'Mon' => 'Пн',
        'Tuesday' => 'Вторник',
        'Tue' => 'Вт',
        'Wednesday' => 'Среда',
        'Wed' => 'Ср',
        'Thursday' => 'Четверг',
        'Thu' => 'Чт',
        'Friday' => 'Пятница',
        'Fri' => 'Пт',
        'Saturday' => 'Суббота',
        'Sat' => 'Сб',
        'Sunday' => 'Воскресенье',
        'Sun' => 'Вс',
        'January' => 'Января',
        'Jan' => 'Янв',
        'February' => 'Февраля',
        'Feb' => 'Фев',
        'March' => 'Марта',
        'Mar' => 'Мар',
        'April' => 'Апреля',
        'Apr' => 'Апр',
        'May' => 'Мая',
        'June' => 'Июня',
        'Jun' => 'Июн',
        'July' => 'Июля',
        'Jul' => 'Июл',
        'August' => 'Августа',
        'Aug' => 'Авг',
        'September' => 'Сентября',
        'Sep' => 'Сен',
        'October' => 'Октября',
        'Oct' => 'Окт',
        'November' => 'Ноября',
        'Nov' => 'Ноя',
        'December' => 'Декабря',
        'Dec' => 'Дек',
        'st' => 'ое',
        'nd' => 'ое',
        'rd' => 'е',
        'th' => 'ое',
    ];

    if (func_num_args() > 1) {
        $timestamp = func_get_arg(1);
        return strtr(date(func_get_arg(0), $timestamp), $translate);
    } else {
        return strtr(date(func_get_arg(0)), $translate);
    }
}
