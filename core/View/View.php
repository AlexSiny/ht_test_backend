<?php

namespace Core\View;

class View
{
    private $title = '';

    private $layout;

    private $pathToTemplates;

    public function __construct(string $pathToTemplates, string $layout)
    {
        $this->pathToTemplates = $pathToTemplates;
        $this->layout = $layout;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setLayout(string $name)
    {
        $this->layout = $name;

        return $this;
    }

    public function render(string $view, array $params = [])
    {
        $viewPath = $this->getTemplatePath($view);
        $viewContent = $this->fetch($viewPath, $params);

        $params['viewContent'] = $viewContent;

        $layoutPath = $this->getTemplatePath("layouts.{$this->layout}");
        $layoutContent = $this->fetch($layoutPath, $params);

        echo $layoutContent;
    }

    public function element(string $element, array $params = [])
    {
        $elementPath = $this->getTemplatePath("elements.{$element}");

        return $this->fetch($elementPath, $params);
    }

    private function getTemplatePath(string $name): string
    {
        $name = $this->normalizeTemplateName($name);

        return $this->pathToTemplates . $name . '.php';
    }

    private function normalizeTemplateName(string $name): string
    {
        return str_replace('.', '/', $name);
    }

    private function fetch(string $path, array $params = []): string
    {
        extract($params);
        ob_start();
        require $path;
        return ob_get_clean();
    }
}
