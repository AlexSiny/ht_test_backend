<?php

namespace Core\Database\Repositories;

interface SQLiteRepositoryInterface extends RepositoryInterface
{
    public function getTableName(): string;
}
