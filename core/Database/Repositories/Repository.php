<?php

namespace Core\Database\Repositories;

use Core\Database\EntityInterface;
use Core\Database\Exceptions\InvalidEntityTypeException;

abstract class Repository implements RepositoryInterface
{
    protected function createEntity(array $attributes): EntityInterface
    {
        $entityClass = $this->getEntityClass();

        return new $entityClass($attributes);
    }

    /**
     * @param EntityInterface[] $entities
     * @return array
     */
    protected function processingEntitiesForSave(array $entities): array
    {
        $processed = [];

        foreach ($entities as $entity) {
            /** @var EntityInterface $entity */
            $processed[$entity->getId()] = $this->processingEntityForSave($entity);
        }

        return $processed;
    }

    protected function processingEntityForSave(EntityInterface $entity): array
    {
        return $entity->toArray();
    }

    protected function checkEntityType(EntityInterface $entity)
    {
        $entityClass = $this->getEntityClass();

        if ($entity instanceof $entityClass) {
            return;
        }

        throw new InvalidEntityTypeException(
            sprintf(
                'Репозиторий "%s" работает только с записями имеющими тип "%s". Получена запись с типом "%s".',
                get_class($this),
                $this->getEntityClass(),
                get_class($entity)
            )
        );
    }
}
