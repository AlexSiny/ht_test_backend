<?php

namespace Core\Database\Repositories;

use Core\Database\Connectors\SQLiteConnection;
use Core\Database\EntityInterface;
use Core\Database\Exceptions\DuplicateEntityIdentifierException;
use Core\Database\Exceptions\EmptyEntityPrimaryKeyException;
use Core\Database\Exceptions\EntityNotFoundException;

abstract class SQLiteRepository extends Repository implements SQLiteRepositoryInterface
{
    protected $connection;

    public function __construct(SQLiteConnection $connection)
    {
        $this->connection = $connection;
    }

    public function truncate()
    {
        $query = "DELETE FROM {$this->getTableName()}";
        $this->getPdo()->exec($query);
    }

    public function getById($id): ?EntityInterface
    {
        $entityClass = $this->getEntityClass();
        $primaryKey = $entityClass::getPrimaryKey();

        $query = "SELECT * FROM {$this->getTableName()} WHERE {$primaryKey} = '{$id}'";

        $entityData = $this->getPdo()
            ->query($query)
            ->fetchAll(\PDO::FETCH_ASSOC);

        if (empty($entityData)) {
            return null;
        }

        return $this->createEntity(current($entityData));
    }

    public function add(EntityInterface $entity)
    {
        $this->checkEntityType($entity);

        if (empty($entity->getId())) {
            $entity->setAttribute($entity::getPrimaryKey(), uniqid());
        }

        if ($this->getById($entity->getId())) {
            throw new DuplicateEntityIdentifierException(
                sprintf('В репозитории уже имеется запись с идентификатором "%s"', $entity->getId())
            );
        }

        $fields = array_map(function ($field) {
            return "'{$field}'";
        }, $this->processingEntityForSave($entity));

        $columns = implode(',', array_keys($fields));
        $values = implode(',', $fields);

        $query = "INSERT INTO {$this->getTableName()} ({$columns}) VALUES ({$values})";

        $this->getPdo()->exec($query);
    }

    public function update(EntityInterface $entity)
    {
        $this->checkEntityType($entity);

        if (empty($entity->getId())) {
            throw new EmptyEntityPrimaryKeyException(
                'Невозможно обновить запись так как у нее отсутствует идентификатор!'
            );
        }

        if (empty($this->getById($entity->getId()))) {
            throw new EntityNotFoundException(
                sprintf('Запись с идентификатором "%s" отсутствует!', $entity->getId())
            );
        }

        $update = [];

        foreach ($this->processingEntityForSave($entity) as $attr => $value) {
            $update[] = "{$attr} = '{$value}'";
        }

        $update = implode(', ', $update);
        $query = "UPDATE {$this->getTableName()} SET {$update} WHERE {$entity::getPrimaryKey()} = '{$entity->getId()}'";

        $this->getPdo()->exec($query);
    }

    public function delete(EntityInterface $entity)
    {
        $this->checkEntityType($entity);

        if (empty($entity->getId())) {
            return;
        }

        $query = "DELETE FROM {$this->getTableName()} WHERE {$entity::getPrimaryKey()} = '{$entity->getId()}'";

        $this->getPdo()->exec($query);
    }

    /**
     * @return EntityInterface[]
     */
    public function getAll(): array
    {
        $query = "SELECT * FROM {$this->getTableName()}";

        $raw = $this->getPdo()
            ->query($query)
            ->fetchAll(\PDO::FETCH_ASSOC);

        $entities = [];

        foreach ($raw as $entity) {
            $entities[] = $this->createEntity($entity);
        }

        return $entities;
    }

    protected function getPdo(): \PDO
    {
        return $this->connection->getPdo();
    }
}
