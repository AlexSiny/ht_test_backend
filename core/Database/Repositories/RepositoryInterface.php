<?php

namespace Core\Database\Repositories;

use Core\Database\EntityInterface;

interface RepositoryInterface
{
    public function getEntityClass(): string;

    public function getById($id): ?EntityInterface;

    public function truncate();

    public function add(EntityInterface $entity);

    public function update(EntityInterface $entity);

    public function delete(EntityInterface $entity);

    /**
     * @return EntityInterface[]
     */
    public function getAll(): array;
}
