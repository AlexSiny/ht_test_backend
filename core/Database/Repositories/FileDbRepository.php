<?php

namespace Core\Database\Repositories;

use Core\Database\Connectors\FileDbConnection;
use Core\Database\EntityInterface;
use Core\Database\Exceptions\DuplicateEntityIdentifierException;
use Core\Database\Exceptions\EmptyEntityPrimaryKeyException;
use Core\Database\Exceptions\EntityNotFoundException;

abstract class FileDbRepository extends Repository implements FileDbRepositoryInterface
{
    protected $connection;

    public function __construct(FileDbConnection $connection)
    {
        $this->connection = $connection;
    }

    public function getById($id): ?EntityInterface
    {
        $entities = $this->getAll();

        return $entities[$id] ?? null;
    }

    public function truncate()
    {
        $this->getFile()->write([]);
    }

    public function add(EntityInterface $entity)
    {
        $this->checkEntityType($entity);

        if (empty($entity->getId())) {
            $entity->setAttribute($entity::getPrimaryKey(), uniqid());
        }

        $entities = $this->getAll();

        if (isset($entities[$entity->getId()])) {
            throw new DuplicateEntityIdentifierException(
                sprintf('В репозитории уже имеется запись с идентификатором "%s"', $entity->getId())
            );
        }

        $entities[$entity->getId()] = $entity;

        $this->getFile()->write($this->processingEntitiesForSave($entities));
    }

    public function update(EntityInterface $entity)
    {
        $this->checkEntityType($entity);

        if (empty($entity->getId())) {
            throw new EmptyEntityPrimaryKeyException(
                'Невозможно обновить запись так как у нее отсутствует идентификатор!'
            );
        }

        $entities = $this->getAll();

        if (!isset($entities[$entity->getId()])) {
            throw new EntityNotFoundException(
                sprintf('Запись с идентификатором "%s" отсутствует!', $entity->getId())
            );
        }

        $entities[$entity->getId()] = $entity;

        $this->getFile()->write($this->processingEntitiesForSave($entities));
    }

    public function delete(EntityInterface $entity)
    {
        $this->checkEntityType($entity);

        if (empty($entity->getId())) {
            return;
        }

        $entities = $this->getAll();

        if (!isset($entities[$entity->getId()])) {
            return;
        }

        unset($entities[$entity->getId()]);

        $this->getFile()->write($this->processingEntitiesForSave($entities));
    }

    /**
     * @return EntityInterface[]
     */
    public function getAll(): array
    {
        $entitiesData = $this->getFile()->read();

        if (empty($entitiesData)) {
            return [];
        }

        $entities = [];

        foreach ($entitiesData as $key => $attributes) {
            $entities[$key] = $this->createEntity($attributes);
        }

        return $entities;
    }
    
    protected function getFile()
    {
        return $this->connection->getFile($this->getFileName());
    }
}
