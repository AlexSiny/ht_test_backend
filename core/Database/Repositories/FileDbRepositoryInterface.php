<?php

namespace Core\Database\Repositories;

interface FileDbRepositoryInterface extends RepositoryInterface
{
    public function getFileName(): string;
}
