<?php

namespace Core\Database\Connectors;

interface SQLiteConnectionInterface
{
    public function getPdo(): \PDO;
}
