<?php

namespace Core\Database\Connectors;

use Core\Utility\Config;

class SQLiteConnection implements SQLiteConnectionInterface
{
    private static $instance;

    private $pdo;

    public static function getInstance(): self
    {
        if (self::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function getPdo(): \PDO
    {
        return $this->pdo;
    }

    private function __construct()
    {
        $this->createPdoFromConfiguration();
    }

    private function createPdoFromConfiguration()
    {
        $config = new Config('database');
        $connectionParams = $config->get('connections', []);

        if (!isset($connectionParams['sqlite'])) {
            throw new \RuntimeException('Конфигурация для SQLite базы данных отсутствует!');
        }

        $pathToDatabase = $connectionParams['sqlite']['path'];

        if (!file_exists($pathToDatabase)) {
            mkdir($pathToDatabase, 0755, true);
        }

        $pathToDatabase = realpath($pathToDatabase);
        $database = $connectionParams['sqlite']['database'];

        $this->pdo = new \PDO("sqlite:{$pathToDatabase}/{$database}");

        if (!empty($connectionParams['sqlite']['attributes'])) {
            foreach ($connectionParams['sqlite']['attributes'] as $key => $val) {
                $this->pdo->setAttribute($key, $val);
            }
        }
    }

    private function __clone()
    {
    }
}
