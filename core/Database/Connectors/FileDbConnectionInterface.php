<?php

namespace Core\Database\Connectors;

use Core\Database\DbFile;

interface FileDbConnectionInterface
{
    public function getFile(string $fileName): DbFile;
}
