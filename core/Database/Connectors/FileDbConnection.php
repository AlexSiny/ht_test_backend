<?php

namespace Core\Database\Connectors;

use Core\Database\DbFile;
use Core\Utility\Config;

class FileDbConnection implements FileDbConnectionInterface
{
    private static $instance;

    /**
     * @var DbFile[]
     */
    private $files = [];

    /**
     * @var string
     */
    private $pathToFiles;

    public static function getInstance(): self
    {
        if (self::$instance === null) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public function getFile(string $fileName): DbFile
    {
        if (!isset($this->files[$fileName])) {
            $path = "{$this->pathToFiles}/{$fileName}";
            $dbFile = new DbFile($path);

            $this->files[$fileName] = $dbFile;
        }

        return $this->files[$fileName];
    }

    private function __construct()
    {
        $this->setPathToFilesFromConfiguration();
    }

    private function setPathToFilesFromConfiguration()
    {
        $config = new Config('database');
        $connectionParams = $config->get('connections', []);

        if (!isset($connectionParams['file_db'])) {
            throw new \RuntimeException('Конфигурация для файловой базы данных отсутствует!');
        }

        $pathToFiles = rtrim($connectionParams['file_db']['path']) . '/' . $connectionParams['file_db']['database'];

        if (!file_exists($pathToFiles)) {
            mkdir($pathToFiles, 0755, true);
        }

        $this->pathToFiles = realpath($pathToFiles);
    }

    private function __clone()
    {
    }
}
