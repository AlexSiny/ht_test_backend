<?php

namespace Core\Database\Exceptions;

class InvalidEntityTypeException extends \LogicException
{
}
