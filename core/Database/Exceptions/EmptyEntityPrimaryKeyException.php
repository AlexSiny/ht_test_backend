<?php

namespace Core\Database\Exceptions;

class EmptyEntityPrimaryKeyException extends \RuntimeException
{
}
