<?php

namespace Core\Database\Exceptions;

class DuplicateEntityIdentifierException extends \LogicException
{
}
