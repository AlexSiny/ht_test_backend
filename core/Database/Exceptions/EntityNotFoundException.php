<?php

namespace Core\Database\Exceptions;

class EntityNotFoundException extends \RuntimeException
{
}
