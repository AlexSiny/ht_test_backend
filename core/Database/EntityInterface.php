<?php

namespace Core\Database;

interface EntityInterface
{
    public function getId();

    public static function getPrimaryKey(): string;

    public function getAvailableAttributes(): array;

    public function getAttributes(): array;

    public function setAttributes(array $attributes);

    public function getAttribute(string $key, $default);

    public function setAttribute(string $key, $val);

    public function toArray(): array;
}
