<?php

namespace Core\Database;

abstract class Entity implements EntityInterface
{
    protected $attributes = [];

    public function __construct(array $attributes = [])
    {
        $this->setAttributes($attributes);
    }

    public function getId()
    {
        return $this->getAttribute(static::getPrimaryKey());
    }

    public function setAttributes(array $attributes)
    {
        foreach ($attributes as $key => $val) {
            $this->setAttribute($key, $val);
        }
    }

    public function getAttributes(): array
    {
        return $this->attributes;
    }

    public function getAttribute(string $key, $default = null)
    {
        return $this->attributes[$key] ?? $default;
    }

    public function setAttribute(string $key, $val)
    {
        if (!in_array($key, $this->getAvailableAttributes())) {
            throw new \InvalidArgumentException(
                sprintf('У записи "%s" отсутствует атрибут с ключем "%s".', get_class($this), $key)
            );
        }

        $this->attributes[$key] = $val;
    }

    public function toArray(): array
    {
        $result = [];

        foreach ($this->getAvailableAttributes() as $key) {
            $result[$key] = $this->getAttribute($key);
        }

        return $result;
    }
}
