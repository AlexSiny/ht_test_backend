<?php

namespace Core\Database;

class DbFile
{
    private $path;

    public function __construct(string $path)
    {
        $this->path = $path;

        if (!$this->exists()) {
            $this->create();
        }
    }

    public function exists()
    {
        return (file_exists($this->path) && is_file($this->path));
    }

    public function create()
    {
        $dir = dirname($this->path);

        if (!file_exists($dir)) {
            mkdir($dir, 0755, true);
        }

        if (!$this->exists()) {
            touch($this->path);
            chmod($this->path, 0755);
        }
    }

    public function read()
    {
        $data = file_get_contents($this->path);

        return unserialize($data);
    }

    public function write(array $data)
    {
        $data = serialize($data);
        file_put_contents($this->path, $data);
    }
}
