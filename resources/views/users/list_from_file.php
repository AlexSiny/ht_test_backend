<?php
/**
 * @var array $users
 * @var \Core\View\View $this
 */
?>

<?=$this->element('users.nav')?>

<?=$this->element('users.table', [
    'users' => $users,
    'removeRoute' => 'users.file.remove',
])?>

<div class="container-fluid">
    <a class="btn btn-success float-right"
       href="<?=route('users.file.generate')?>"
       role="button">
        Сгенерировать в файле
    </a>
</div>
