<?php
/**
 * @var array $users
 * @var \Core\View\View $this
 */
?>

<?=$this->element('users.nav')?>

<?=$this->element('users.table', [
    'users' => $users,
    'removeRoute' => 'users.db.remove',
])?>

<div class="container-fluid">
    <a class="btn btn-success float-right"
       href="<?=route('users.db.generate')?>"
       role="button">
        Сгенерировать в базе
    </a>
</div>
