<?php
/**
 * @var \Core\View\View $this
 */

$currentRouteName = request()->getLoadedRoute()->getName();

$nav = [
    'users.file.list' => 'Файл',
    'users.db.list' => 'База данных',
];
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <ul class="nav nav-pills">
        <?php foreach ($nav as $routeName => $title) : ?>
            <li class="nav-item">
                <?php if ($currentRouteName == $routeName) : ?>
                    <span class="nav-link active">
                        <?=$title?>
                    </span>
                <?php else : ?>
                    <a class="nav-link" href="<?=route($routeName)?>">
                        <?=$title?>
                    </a>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
    </ul>
</nav>