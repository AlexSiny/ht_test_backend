<?php
/**
 * @var \App\Entities\User[] $users
 * @var string $removeRoute
 * @var \Core\View\View $this
 */
?>
<table class="table">
    <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">Имя</th>
            <th scope="col">Фамилия</th>
            <th scope="col">Телефон</th>
            <th scope="col">Email</th>
            <th scope="col">Адрес</th>
            <th scope="col">Зарегистрирован</th>
            <th scope="col"></th>
        </tr>
    </thead>
    <tbody>
        <?php $number = 1; ?>
        <?php foreach ($users as $user) : ?>
            <tr>
                <th scope="row"><?=$number++?></th>
                <td><?=$user->getAttribute('first_name')?></td>
                <td><?=$user->getAttribute('last_name')?></td>
                <td><?=$user->getAttribute('phone')?></td>
                <td><?=$user->getAttribute('email')?></td>
                <td>
                    <?php if ($user->getLocation()) : ?>
                        <?=$user->getLocation()->getAddress()?>
                    <?php endif; ?>
                </td>
                <td>
                    <?php $registrationDateTime = $user->getAttribute('registered_at'); ?>
                    <?php if ($registrationDateTime) : ?>
                        <?=mb_strtolower(rus_date('j F Y H:i:s', $registrationDateTime->getTimestamp())) ?>
                    <?php endif; ?>
                </td>
                <td>
                    <a href="<?=route($removeRoute, ['uuid' => $user->getId()])?>"
                       class="btn btn-danger">
                        Удалить
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
    </tbody>
</table>