<?php

use Pecee\SimpleRouter\SimpleRouter as Router;

Router::setDefaultNamespace('App\Controllers');

Router::get('/', 'UsersController@showFromFileDbStorage')
    ->setName('users.file.list');

Router::get('/users/remove_from_file/{uuid}/', 'UsersController@removeFromFileDbStorage')
    ->setName('users.file.remove');

Router::get('/users/generate_file', 'UsersController@generateForFileDbStorage')
    ->setName('users.file.generate');

Router::get('/users/show_from_db', 'UsersController@showFromSQLiteStorage')
    ->setName('users.db.list');

Router::get('/users/remove_from_db/{uuid}/', 'UsersController@removeFromSQLiteStorage')
    ->setName('users.db.remove');

Router::get('/users/generate_database', 'UsersController@generateForSQLiteStorage')
    ->setName('users.db.generate');
