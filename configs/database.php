<?php

return [
    'connections' => [
        'sqlite' => [
            'database' => 'db.sqlite3',
            'attributes' => [
                \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            ],
            'path' => databases_path('sqlite')
        ],
        'file_db' => [
            'database' => 'default',
            'path' => databases_path('file_db')
        ],
    ],
];
